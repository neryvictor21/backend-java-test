INSERT INTO LOCALIZACAO (latitude, longitude, nome) VALUES ('-7.125337', '-34.856657', 'marte');
INSERT INTO REBELDE (nome, idade, genero, localizacao_id) VALUES ('Victor Nery', 22, 'HOMEM', 1);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (2, 'ARMA', 1);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (1, 'AGUA', 1);


INSERT INTO LOCALIZACAO (latitude, longitude, nome) VALUES ('-2.121237', '-21.850234', 'jupiter');
INSERT INTO REBELDE (nome, idade, genero, localizacao_id) VALUES ('Alice Lopes', 22, 'MULHER', 2);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (3, 'MUNICAO', 2);


INSERT INTO LOCALIZACAO (latitude, longitude, nome) VALUES ('-1.536922', '-27.568324', 'saturno');
INSERT INTO REBELDE (nome, idade, genero, localizacao_id) VALUES ('Ian Nilo', 20, 'HOMEM', 3);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (4, 'ARMA', 3);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (7, 'COMIDA', 3);


INSERT INTO LOCALIZACAO (latitude, longitude, nome) VALUES ('-9.425675', '-16.453280', 'mercurio');
INSERT INTO REBELDE (nome, idade, genero, localizacao_id) VALUES ('Analia Dornellas', 25, 'MULHER', 4);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (2, 'AGUA', 4);
INSERT INTO ITEM (quantidade, tipo_item, rebelde_id) VALUES (4, 'MUNICAO', 4);


INSERT INTO REBELDE_ACUSADORES (rebelde_id, acusadores_id) VALUES (4, 1);
INSERT INTO REBELDE_ACUSADORES (rebelde_id, acusadores_id) VALUES (4, 2);
INSERT INTO REBELDE_ACUSADORES (rebelde_id, acusadores_id) VALUES (4, 3);
INSERT INTO REBELDE_ACUSADORES (rebelde_id, acusadores_id) VALUES (2, 3);