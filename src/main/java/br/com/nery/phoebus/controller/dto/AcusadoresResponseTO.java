package br.com.nery.phoebus.controller.dto;

import br.com.nery.phoebus.domain.Rebelde;

public class AcusadoresResponseTO {

	private Long id;
	private String nome;
	
	public AcusadoresResponseTO(Rebelde rebelde) {
		this.id = rebelde.getId();
		this.nome = rebelde.getNome();
	}

	public Long getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}

}