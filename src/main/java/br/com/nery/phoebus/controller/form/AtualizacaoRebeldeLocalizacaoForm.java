package br.com.nery.phoebus.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.nery.phoebus.domain.Localizacao;
import br.com.nery.phoebus.domain.Rebelde;
import br.com.nery.phoebus.repository.RebeldeRepository;

public class AtualizacaoRebeldeLocalizacaoForm {
	
	@NotNull @NotEmpty
	private String latitude;
	
	@NotNull @NotEmpty
	private String longitude;
	
	@NotNull @NotEmpty
	private String nome;

	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Rebelde atualizar(Long id, RebeldeRepository rebeldeRepository) {
		Rebelde rebelde = rebeldeRepository.getOne(id);
		Localizacao localizacao = rebelde.getLocalizacao();
		localizacao.setLatitude(this.latitude);
		localizacao.setLongitude(this.longitude);
		localizacao.setNome(this.nome);
		
		return rebelde;
	}
	
}