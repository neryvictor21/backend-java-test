package br.com.nery.phoebus.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.nery.phoebus.controller.dto.LocalizacaoRequestTO;
import br.com.nery.phoebus.controller.dto.NegociacaoRequestTO;
import br.com.nery.phoebus.controller.dto.RebeldeRequestTO;
import br.com.nery.phoebus.controller.dto.RebeldeResponseTO;
import br.com.nery.phoebus.controller.dto.TraidorRequestTO;
import br.com.nery.phoebus.domain.Localizacao;
import br.com.nery.phoebus.domain.Rebelde;
import br.com.nery.phoebus.repository.RebeldeRepository;
import br.com.nery.phoebus.services.RebeldeService;

@RestController
@RequestMapping("/rebeldes")
public class RebeldeController {

	@Autowired
	private RebeldeRepository rebeldeRepository;

	@Autowired
	private RebeldeService rebeldeService;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping
	public List<RebeldeResponseTO> lista() {
		List<Rebelde> rebeldes = rebeldeService.findAll();
		return RebeldeResponseTO.converter(rebeldes);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<RebeldeResponseTO> cadastrar(@RequestBody @Valid RebeldeRequestTO rebeldeRequestTO, UriComponentsBuilder uriBuilder) {
		Rebelde rebelde = rebeldeService.create(modelMapper.map(rebeldeRequestTO, Rebelde.class));
		URI uri = uriBuilder.path("/rebeldes/{id}").buildAndExpand(rebelde.getId()).toUri();
		return ResponseEntity.created(uri).body(new RebeldeResponseTO(rebelde));
	}

	@PutMapping("/{id}/reportarLocalizacao")
	@Transactional
	public ResponseEntity<RebeldeResponseTO> reportarLocalizacao(@PathVariable Long id, @RequestBody @Valid LocalizacaoRequestTO localizacaoRequestTO) {
		Rebelde rebelde = rebeldeService.reportarLocalizacao(id, modelMapper.map(localizacaoRequestTO, Localizacao.class));
		return ResponseEntity.ok(new RebeldeResponseTO(rebelde));
	}

	@PutMapping("/{id}/reportarTraidor")
	@Transactional
	public ResponseEntity<RebeldeResponseTO> reportarTraidor(@PathVariable Long id, @RequestBody TraidorRequestTO traidorRequestTO) {
		Rebelde rebelde = rebeldeService.reportarTraidor(id, traidorRequestTO);
		return ResponseEntity.ok(new RebeldeResponseTO(rebelde));
	}

	@PutMapping("/negociar")
	@Transactional
	public void negociar(@RequestBody @Valid NegociacaoRequestTO negociacaoRequestTO) {
		rebeldeService.negociar(negociacaoRequestTO);
	}

	@GetMapping("/{id}")
	public ResponseEntity<RebeldeResponseTO> detalhar(@PathVariable Long id) {
		Optional<Rebelde> rebelde = rebeldeRepository.findById(id);
		if (rebelde.isPresent()) {
			return ResponseEntity.ok(new RebeldeResponseTO(rebelde.get()));
		}

		return ResponseEntity.notFound().build();
	}

}