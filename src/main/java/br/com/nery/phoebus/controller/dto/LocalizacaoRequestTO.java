package br.com.nery.phoebus.controller.dto;

import javax.validation.constraints.NotEmpty;

import br.com.nery.phoebus.domain.Localizacao;

public class LocalizacaoRequestTO {

	@NotEmpty
	private String latitude;
	
	@NotEmpty
	private String longitude;
	
	@NotEmpty
	private String nome;
	
	public LocalizacaoRequestTO() {
		
	}
	
	public LocalizacaoRequestTO(Localizacao localizacao) {
		this.latitude = localizacao.getLatitude();
		this.longitude = localizacao.getLongitude();
		this.nome = localizacao.getNome();
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getNome() {
		return nome;
	}
	
}