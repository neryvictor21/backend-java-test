package br.com.nery.phoebus.controller.dto;

public class RelatorioPontosResponseTO {

	private int pontos;
	
	public RelatorioPontosResponseTO() {
		
	}
	
	public RelatorioPontosResponseTO(int pontos) {
		this.pontos = pontos;
	}

	public int getPontos() {
		return pontos;
	}
	
}