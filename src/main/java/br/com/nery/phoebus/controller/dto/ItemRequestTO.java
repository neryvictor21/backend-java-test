package br.com.nery.phoebus.controller.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import br.com.nery.phoebus.domain.Item;
import br.com.nery.phoebus.domain.TipoItem;

public class ItemRequestTO {

	@NotNull @Range(min = 1, max = 50)
	private int quantidade;
	
	@NotNull @Valid
	private TipoItem tipoItem;
	
	public ItemRequestTO() {
		
	}
	
	public ItemRequestTO(Item item) {
		this.quantidade = item.getQuantidade();
		this.tipoItem = item.getTipoItem();
	}

	public int getQuantidade() {
		return quantidade;
	}
	public TipoItem getTipoItem() {
		return tipoItem;
	}
	
	public static Item converter(ItemRequestTO item) {
		return new Item(item.getQuantidade(), item.getTipoItem());
	}
	
}