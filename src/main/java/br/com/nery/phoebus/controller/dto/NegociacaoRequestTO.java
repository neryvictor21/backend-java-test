package br.com.nery.phoebus.controller.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class NegociacaoRequestTO {

	private Long idNegociador;
	
	@NotNull @NotEmpty
	private List<ItemRequestTO> inventarioNegociador;
	
	private Long idInteressado;
	
	@NotNull @NotEmpty
	private List<ItemRequestTO> inventarioInteressado;
	
	public NegociacaoRequestTO(Long idNegociador, Long idInteressado, List<ItemRequestTO> inventarioNegociador, List<ItemRequestTO> inventarioInteressado) {
		this.idNegociador = idNegociador;
		this.inventarioNegociador = inventarioNegociador;
		
		this.idInteressado = idInteressado;
		this.inventarioInteressado = inventarioInteressado;
	}

	public Long getIdNegociador() {
		return idNegociador;
	}
	public List<ItemRequestTO> getInventarioNegociador() {
		return inventarioNegociador;
	}
	public Long getIdInteressado() {
		return idInteressado;
	}
	public List<ItemRequestTO> getInventarioInteressado() {
		return inventarioInteressado;
	}
	
}