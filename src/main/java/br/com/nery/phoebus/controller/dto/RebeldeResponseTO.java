package br.com.nery.phoebus.controller.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.nery.phoebus.domain.Rebelde;

public class RebeldeResponseTO {

	private Long id;
	private String nome;
	private int idade;
	private String genero;
	private LocalizacaoResponseTO localizacao;
	private List<ItemResponseTO> inventario;
	
	private List<AcusadoresResponseTO> acusadores;
	
	public RebeldeResponseTO(Rebelde rebelde) {
		this.id = rebelde.getId();
		this.nome = rebelde.getNome();
		this.idade = rebelde.getIdade();
		this.genero = rebelde.getGenero().getNome();
		this.localizacao = new LocalizacaoResponseTO(rebelde.getLocalizacao());
		this.inventario = new ArrayList<ItemResponseTO>();
		this.inventario.addAll(rebelde.getInventario().stream().map(ItemResponseTO::new).collect(Collectors.toList()));
		this.acusadores = new ArrayList<AcusadoresResponseTO>();
		if (rebelde.getAcusadores() != null && rebelde.getAcusadores().size() > 0) {
			this.acusadores.addAll(rebelde.getAcusadores().stream().map(AcusadoresResponseTO::new).collect(Collectors.toList()));			
		}
	}

	public Long getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public int getIdade() {
		return idade;
	}
	public String getGenero() {
		return genero;
	}
	public LocalizacaoResponseTO getLocalizacao() {
		return localizacao;
	}
	public List<ItemResponseTO> getInventario() {
		return inventario;
	}
	public List<AcusadoresResponseTO> getAcusadores() {
		return acusadores;
	}

	public static List<RebeldeResponseTO> converter(List<Rebelde> rebeldes) {
		return rebeldes.stream().map(RebeldeResponseTO::new).collect(Collectors.toList());
	}

}