package br.com.nery.phoebus.controller.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.nery.phoebus.domain.Rebelde;

public class RebeldeRequestTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@NotNull @NotEmpty @Length(min = 4)
	private String nome;

	@NotNull
	private int idade;
	
	@NotNull @NotEmpty
	private String genero;
	
	@NotNull @Valid
	private LocalizacaoRequestTO localizacao;
	
	@NotNull @NotEmpty @Valid
	private List<ItemRequestTO> inventario;
	
	public RebeldeRequestTO() {
		
	}
	
	public RebeldeRequestTO(Rebelde rebelde) {
		this.nome = rebelde.getNome();
		this.idade = rebelde.getIdade();
		this.genero = rebelde.getGenero().getNome();
		this.localizacao = new LocalizacaoRequestTO(rebelde.getLocalizacao());
		this.inventario = new ArrayList<ItemRequestTO>();
		this.inventario.addAll(rebelde.getInventario().stream().map(ItemRequestTO::new).collect(Collectors.toList()));
	}

	public String getNome() {
		return nome;
	}
	public int getIdade() {
		return idade;
	}
	public String getGenero() {
		return genero;
	}
	public LocalizacaoRequestTO getLocalizacao() {
		return localizacao;
	}
	public List<ItemRequestTO> getInventario() {
		return inventario;
	}

	public static List<RebeldeRequestTO> converter(List<Rebelde> rebeldes) {
		return rebeldes.stream().map(RebeldeRequestTO::new).collect(Collectors.toList());
	}

}