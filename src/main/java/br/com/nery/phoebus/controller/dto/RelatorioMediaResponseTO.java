package br.com.nery.phoebus.controller.dto;

import br.com.nery.phoebus.domain.TipoItem;

public class RelatorioMediaResponseTO {

	private TipoItem tipoItem;

	private double media;
	
	public RelatorioMediaResponseTO() {
		
	}
	
	public RelatorioMediaResponseTO(TipoItem tipoItem, double media) {
		this.tipoItem = tipoItem;
		this.media = media;
	}

	public TipoItem getTipoItem() {
		return tipoItem;
	}
	public double getMedia() {
		return media;
	}
	
}