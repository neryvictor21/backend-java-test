package br.com.nery.phoebus.controller.dto;

import br.com.nery.phoebus.domain.Genero;
import br.com.nery.phoebus.domain.Rebelde;

public class DetalhesRebeldeDTO {

	private Long id;
	private String nome;
	private int idade;
	private Genero genero;
	private String nomeLocalizacao;
	
	public DetalhesRebeldeDTO(Rebelde rebelde) {
		this.id = rebelde.getId();
		this.nome = rebelde.getNome();
		this.idade = rebelde.getIdade();
		this.genero = rebelde.getGenero();
		this.nomeLocalizacao = rebelde.getLocalizacao().getNome();
	}

	public Long getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public int getIdade() {
		return idade;
	}
	public Genero getGenero() {
		return genero;
	}
	public String getNomeLocalizacao() {
		return nomeLocalizacao;
	}
	
}