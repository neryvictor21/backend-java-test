package br.com.nery.phoebus.controller.dto;

public class RelatorioPorcentagemResponseTO {

	private double porcentagem;
	
	public RelatorioPorcentagemResponseTO() {
		
	}
	
	public RelatorioPorcentagemResponseTO(double porcentagem) {
		this.porcentagem = porcentagem;
	}
	
	public double getPorcentagem() {
		return porcentagem;
	}
	
}