package br.com.nery.phoebus.controller.dto;

import java.io.Serializable;

public class TraidorRequestTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;

	public TraidorRequestTO() {
		
	}
	
	public TraidorRequestTO(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

}