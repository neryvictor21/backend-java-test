package br.com.nery.phoebus.controller.dto;

import br.com.nery.phoebus.domain.Item;

public class ItemResponseTO {

	private Long id;
	private int quantidade;
	private String nome;
	private int ponto;
	
	public ItemResponseTO(Item item) {
		this.id = item.getId();
		this.quantidade = item.getQuantidade();
		this.nome = item.getTipoItem().getNome();
		this.ponto = item.getTipoItem().getPonto();
	}

	public Long getId() {
		return id;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public String getNome() {
		return nome;
	}
	public int getPonto() {
		return ponto;
	}
	
}