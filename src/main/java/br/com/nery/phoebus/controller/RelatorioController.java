package br.com.nery.phoebus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.nery.phoebus.controller.dto.RelatorioMediaResponseTO;
import br.com.nery.phoebus.controller.dto.RelatorioPontosResponseTO;
import br.com.nery.phoebus.controller.dto.RelatorioPorcentagemResponseTO;
import br.com.nery.phoebus.services.RelatorioService;

@RestController
@RequestMapping("/relatorios")
public class RelatorioController {
	
	@Autowired
	private RelatorioService relatorioService;
	
	@GetMapping("/porcentagemRebeldes")
	public ResponseEntity<RelatorioPorcentagemResponseTO> obterPorcentagemRebeldes() {
		return ResponseEntity.ok(relatorioService.getPorcentagemRebeldeTraidor(false));
	}
	
	@GetMapping("/porcentagemTraidores")
	public ResponseEntity<RelatorioPorcentagemResponseTO> obterPorcentagemTraidores() {
		return ResponseEntity.ok(relatorioService.getPorcentagemRebeldeTraidor(true));
	}
	
	@GetMapping("/mediaTipoRecurso")
	public ResponseEntity<List<RelatorioMediaResponseTO>> obterMediaTipoRecursoPorRebelde() {
		return ResponseEntity.ok(relatorioService.obterMediaTipoRecursoPorRebelde());
	}

	@GetMapping("/pontosPerdidos")
	public ResponseEntity<RelatorioPontosResponseTO> obterPontosPerdidos() {
		return ResponseEntity.ok(relatorioService.obterPontosPerdidosDevidoTraidores());
	}
	
}