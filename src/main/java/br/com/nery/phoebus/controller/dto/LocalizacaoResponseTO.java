package br.com.nery.phoebus.controller.dto;

import br.com.nery.phoebus.domain.Localizacao;

public class LocalizacaoResponseTO {

	private Long id;
	private String latitude;
	private String longitude;
	private String nome;
	
	public LocalizacaoResponseTO(Localizacao localizacao) {
		this.id = localizacao.getId();
		this.latitude = localizacao.getLatitude();
		this.longitude = localizacao.getLongitude();
		this.nome = localizacao.getNome();
	}
	
	public Long getId() {
		return id;
	}
	public String getLatitude() {
		return latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public String getNome() {
		return nome;
	}
	
}