package br.com.nery.phoebus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoebusApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhoebusApplication.class, args);
	}

}
