package br.com.nery.phoebus.services.exception;

public class RebeldeException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private String campo;
	private String mensagem;
	
	public RebeldeException(String campo, String mensagem) {
		this.campo = campo;
		this.mensagem = mensagem;
	}

	public String getCampo() {
		return campo;
	}

	public String getMensagem() {
		return mensagem;
	}

}
