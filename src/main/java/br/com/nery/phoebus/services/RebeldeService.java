package br.com.nery.phoebus.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nery.phoebus.controller.dto.ItemRequestTO;
import br.com.nery.phoebus.controller.dto.NegociacaoRequestTO;
import br.com.nery.phoebus.controller.dto.TraidorRequestTO;
import br.com.nery.phoebus.domain.Item;
import br.com.nery.phoebus.domain.Localizacao;
import br.com.nery.phoebus.domain.Rebelde;
import br.com.nery.phoebus.repository.ItemRepository;
import br.com.nery.phoebus.repository.RebeldeRepository;
import br.com.nery.phoebus.services.exception.RebeldeException;

@Service
public class RebeldeService {

	@Autowired
	private RebeldeRepository rebeldeRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	public List<Rebelde> findAll() {
		List<Rebelde> rebeldes = rebeldeRepository.findAll();
		return rebeldes;
	}
	
	public Rebelde create(Rebelde rebelde) {
		rebeldeRepository.save(rebelde);
		for (Item item: rebelde.getInventario()) {
			item.setRebelde(rebelde);
			itemRepository.save(item);
		}
		return rebelde;
	}
	
	public Rebelde reportarLocalizacao(Long id, Localizacao localizacao) {
		Rebelde rebelde = null;
		Optional<Rebelde> optional = rebeldeRepository.findById(id);
		
		if (!optional.isPresent()) {
			throw new RebeldeException("Id","Rebelde com id " + id + " não existe");
		}
		
		rebelde = optional.get();
		rebelde.getLocalizacao().setLatitude(localizacao.getLatitude());
		rebelde.getLocalizacao().setLongitude(localizacao.getLongitude());
		rebelde.getLocalizacao().setNome(localizacao.getNome());
		
		return rebelde;
	}
	
	public Rebelde reportarTraidor(Long id, TraidorRequestTO traidorRequestTO) {
		Rebelde rebelde = null;
		
		Optional<Rebelde> optRebelde = rebeldeRepository.findById(id);
		if (!optRebelde.isPresent()) {
			throw new RebeldeException("Id","Rebelde com id " + id + " não existe");
		}
		
		Optional<Rebelde> optTraidor = rebeldeRepository.findById(traidorRequestTO.getId());
		if (!optTraidor.isPresent()) {
			throw new RebeldeException("Id","Rebelde/Traidor com id " + id + " não existe");
		}
			
		rebelde = optTraidor.get();
		for (Rebelde r: rebelde.getAcusadores()) {
			if (r.getId().equals(id)) {
				throw new RebeldeException("Id", "Rebelde com id " + id + " já reportou esse rebelde");
			}
		}
		rebelde.getAcusadores().add(optRebelde.get());
		rebeldeRepository.save(rebelde);
			
		return rebelde;
	}
	
	public void negociar(NegociacaoRequestTO negociacaoRequestTO) {
		if (negociacaoRequestTO.getIdNegociador() == null || negociacaoRequestTO.getIdInteressado() == null) {
			throw new RebeldeException("Id","Id não pode ser nulo");
		}
		
		if (negociacaoRequestTO.getIdNegociador().equals(negociacaoRequestTO.getIdInteressado())) {
			throw new RebeldeException("Id","Id não pode ser igual");
		}
			
		Optional<Rebelde> optNegociador = rebeldeRepository.findById(negociacaoRequestTO.getIdNegociador());
		if (!optNegociador.isPresent()) {
			throw new RebeldeException("Id","Rebelde com id " + negociacaoRequestTO.getIdNegociador() + " não existe");
		}

		Optional<Rebelde> optInteressado = rebeldeRepository.findById(negociacaoRequestTO.getIdInteressado());
		if (!optInteressado.isPresent()) {
			throw new RebeldeException("Id","Rebelde com id " + negociacaoRequestTO.getIdInteressado() + " não existe");
		}
		
		if (optNegociador.get().isTraidor() || optInteressado.get().isTraidor()) {
			throw new RebeldeException("Rebelde","Pelo menos um dos rebeldes não pode negociar pois é traidor.");
		}			

		int pontuacaoTotalRebelde01 = obterPontuacaoTotal(optNegociador.get(), negociacaoRequestTO.getInventarioNegociador());
		int pontuacaoTotalRebelde02 = obterPontuacaoTotal(optInteressado.get(), negociacaoRequestTO.getInventarioInteressado());

		if (pontuacaoTotalRebelde01 != pontuacaoTotalRebelde02) {
			throw new RebeldeException("InventarioRebelde","Ambos os lados devem oferecer a mesma quantidade de pontos.");			
		}
		
		transferirItens(optNegociador.get(), optInteressado.get(), negociacaoRequestTO.getInventarioNegociador());
		transferirItens(optInteressado.get(), optNegociador.get(), negociacaoRequestTO.getInventarioInteressado());
	}
	
	public int obterPontuacaoTotal(Rebelde rebelde, List<ItemRequestTO> inventarioRequest) {
		int sum = 0;
		for (ItemRequestTO itemRequest: inventarioRequest) {
			boolean existItem = false;
			for (Item item: rebelde.getInventario()) {
				if (itemRequest.getTipoItem().equals(item.getTipoItem())) {
					if (itemRequest.getQuantidade() <= item.getQuantidade()) {
						sum += itemRequest.getQuantidade() * itemRequest.getTipoItem().getPonto();
					} else {
						throw new RebeldeException("Item","O rebelde " + rebelde.getId() + " não possui " + itemRequest.getQuantidade() + " item(ns) do tipo " + itemRequest.getTipoItem().getNome());
					}
					existItem = true;
				}
			}
			if (!existItem) {
				throw new RebeldeException("Item","O rebelde " + rebelde.getId() + " não possui o item do tipo " + itemRequest.getTipoItem().getNome());
			}
		}
		return sum;  
	}
	
	public void transferirItens(Rebelde rebeldeOrigem, Rebelde rebeldeDestino, List<ItemRequestTO> inventarioRequest) {
		for (ItemRequestTO itemRequest: inventarioRequest) {
			for (Item item: rebeldeOrigem.getInventario()) {
				if (itemRequest.getTipoItem().equals(item.getTipoItem())) {
					item.setQuantidade(item.getQuantidade() - itemRequest.getQuantidade());
				}
			}
			boolean existItem = false;
			for (Item item: rebeldeDestino.getInventario()) {
				if (itemRequest.getTipoItem().equals(item.getTipoItem())) {
					item.setQuantidade(item.getQuantidade() + itemRequest.getQuantidade());
					existItem = true;
				}
			}
			if (!existItem) {
				Item i = ItemRequestTO.converter(itemRequest);
				i.setRebelde(rebeldeDestino);
				itemRepository.save(i);
			}
		}	
	}
	
}
