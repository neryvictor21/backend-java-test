package br.com.nery.phoebus.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nery.phoebus.controller.dto.RelatorioMediaResponseTO;
import br.com.nery.phoebus.controller.dto.RelatorioPontosResponseTO;
import br.com.nery.phoebus.controller.dto.RelatorioPorcentagemResponseTO;
import br.com.nery.phoebus.domain.Item;
import br.com.nery.phoebus.domain.Rebelde;
import br.com.nery.phoebus.domain.TipoItem;
import br.com.nery.phoebus.repository.RebeldeRepository;

@Service
public class RelatorioService {

	@Autowired
	private RebeldeRepository rebeldeRepository;
	
	public RelatorioPorcentagemResponseTO getPorcentagemRebeldeTraidor(Boolean isTraidor) {
		List<Rebelde> rebeldes = rebeldeRepository.findAll();
		int count = 0;
		for (Rebelde rebelde: rebeldes) {
			if (isTraidor) {
				if (rebelde.isTraidor()) {
					count++;
				}
			} else {
				if (!rebelde.isTraidor()) {
					count++;
				}
			}
		}
		return new RelatorioPorcentagemResponseTO((count*100)/rebeldes.size()	);
	}
	
	public RelatorioPontosResponseTO obterPontosPerdidosDevidoTraidores() {
		int pontos = 0;
		List<Rebelde> rebeldes = rebeldeRepository.findAll();
		for (Rebelde rebelde: rebeldes) {
			if (rebelde.isTraidor()) {
				for (Item item: rebelde.getInventario()) {
					pontos += item.getQuantidade() * item.getTipoItem().getPonto();					
				}
			}
		}
		return new RelatorioPontosResponseTO(pontos);
	}
	
	public List<RelatorioMediaResponseTO> obterMediaTipoRecursoPorRebelde() {
		List<Rebelde> rebeldes = rebeldeRepository.findAll();
		double countArma = 0;
		double countMunicao = 0;
		double countAgua = 0;
		double countComida = 0;
		int countRebeldeIsNotTraidor = 0;
		for (Rebelde rebelde: rebeldes) {
			if (!rebelde.isTraidor()) {
				List<Item> itens = rebelde.getInventario();
				for (Item item: itens) {
					switch (item.getTipoItem()) {
					case ARMA:
						countArma += item.getQuantidade();
						break;
					case MUNICAO:
						countMunicao += item.getQuantidade();
						break;
					case AGUA:
						countAgua += item.getQuantidade();
						break;
					case COMIDA:
						countComida += item.getQuantidade();
						break;
					default:
						break;
					}
				}
				countRebeldeIsNotTraidor++;
			}
		}
		List<RelatorioMediaResponseTO> mediaTipoItem = new ArrayList<RelatorioMediaResponseTO>();
		mediaTipoItem.add(new RelatorioMediaResponseTO(TipoItem.ARMA, countArma/countRebeldeIsNotTraidor));
		mediaTipoItem.add(new RelatorioMediaResponseTO(TipoItem.MUNICAO, countMunicao/countRebeldeIsNotTraidor));
		mediaTipoItem.add(new RelatorioMediaResponseTO(TipoItem.AGUA, countAgua/countRebeldeIsNotTraidor));
		mediaTipoItem.add(new RelatorioMediaResponseTO(TipoItem.COMIDA, countComida/countRebeldeIsNotTraidor));
		return mediaTipoItem;
	}
}
