package br.com.nery.phoebus.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.nery.phoebus.domain.Localizacao;

public interface LocalizacaoRepository extends JpaRepository<Localizacao, Long> {

}
