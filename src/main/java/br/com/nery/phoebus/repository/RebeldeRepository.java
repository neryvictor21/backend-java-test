package br.com.nery.phoebus.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.nery.phoebus.domain.Rebelde;

public interface RebeldeRepository extends JpaRepository<Rebelde, Long> {

	List<Rebelde> findByLocalizacaoNome(String nomeLocalizacao);

}
