package br.com.nery.phoebus.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.nery.phoebus.domain.Item;

public interface ItemRepository extends JpaRepository<Item, Long> {

}
