package br.com.nery.phoebus.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Rebelde {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	private int idade;
	
	@Enumerated(EnumType.STRING)
	private Genero genero;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Localizacao localizacao;
	
	@OneToMany(mappedBy = "rebelde")
	private List<Item> inventario;
	
	@ManyToMany
	private List<Rebelde> acusadores;
	
	public Rebelde() {
	}
	
	public Rebelde(String nome, int idade, Genero genero, Localizacao localizacao, List<Item> inventario, List<Rebelde> acusadores) {
		this.nome = nome;
		this.idade = idade;
		this.genero = genero;
		this.localizacao = localizacao;
		this.inventario = inventario;
		this.acusadores = acusadores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rebelde other = (Rebelde) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public Genero getGenero() {
		return genero;
	}
	public void setGenero(Genero genero) {
		this.genero = genero;
	}
	public Localizacao getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}
	public List<Item> getInventario() {
		return inventario;
	}
	public void setInventario(List<Item> inventario) {
		this.inventario = inventario;
	}
	public List<Rebelde> getAcusadores() {
		return acusadores;
	}
	public void setAcusadores(List<Rebelde> acusadores) {
		this.acusadores = acusadores;
	}

	public boolean isTraidor() {
		return acusadores != null && acusadores.size() > 2;
	}

}