package br.com.nery.phoebus.domain;

public enum Genero {
	
	HOMEM("Homem"),
	MULHER("Mulher");
	
	private final String nome;

	private Genero(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

}
