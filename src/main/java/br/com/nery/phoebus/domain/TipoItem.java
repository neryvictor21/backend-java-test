package br.com.nery.phoebus.domain;

public enum TipoItem {
	
	ARMA("Arma", 4),
	MUNICAO("Munição", 3),
	AGUA("Água", 2),
	COMIDA("Comida", 1);
	
	private final String nome;
	private final int ponto;

	private TipoItem(String nome, int ponto) {
		this.nome = nome;
		this.ponto = ponto;
	}

	public String getNome() {
		return nome;
	}

	public int getPonto() {
		return ponto;
	}
	
}