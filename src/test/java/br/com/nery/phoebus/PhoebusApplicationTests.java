package br.com.nery.phoebus;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.nery.phoebus.domain.Genero;
import br.com.nery.phoebus.domain.Item;
import br.com.nery.phoebus.domain.Localizacao;
import br.com.nery.phoebus.domain.Rebelde;
import br.com.nery.phoebus.domain.TipoItem;
import br.com.nery.phoebus.repository.RebeldeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoebusApplicationTests {

	@Autowired
	RebeldeRepository rebeldeRepository;
	
	@Test
	public void insertRebelde() {
		Rebelde rebelde = generateRebelde(4, TipoItem.AGUA);
		rebeldeRepository.save(rebelde);
	}
	
	@Test
	public void findAllRebelds() {
		List<Rebelde> rebeldes = rebeldeRepository.findAll();
		rebeldes.stream().forEach(rebelde -> System.out.println(rebelde.getId()));
	}
	
	@Test
	public void negociar() {
		Rebelde rebeldeNegociador = generateRebelde(2, TipoItem.MUNICAO);
		Rebelde rebeldeInteressado = generateRebelde(3, TipoItem.COMIDA);
		
		int valorNegociador = rebeldeInteressado.getInventario().get(0).getQuantidade() * rebeldeInteressado.getInventario().get(0).getTipoItem().getPonto();
		int valorInteressado = rebeldeInteressado.getInventario().get(0).getQuantidade() * rebeldeInteressado.getInventario().get(0).getTipoItem().getPonto();
		
		if (valorNegociador == valorInteressado) {
			rebeldeInteressado.getInventario().add(rebeldeNegociador.getInventario().get(0));	
			rebeldeNegociador.getAcusadores().remove(0);
			
			rebeldeNegociador.getInventario().add(rebeldeInteressado.getInventario().get(0));	
			rebeldeInteressado.getAcusadores().remove(0);	
		}
	}
	
	private Rebelde generateRebelde(int quantidade, TipoItem tipoItem) {
		List<Item> inventario = new ArrayList<Item>();
		inventario.add(new Item(3, tipoItem));
		
		Localizacao localizacao = new Localizacao();
		localizacao.setLatitude("-21.023130");
		localizacao.setLatitude("-9.589234");
		localizacao.setNome("Marte");
		
		Rebelde rebelde = new Rebelde();
		rebelde.setNome("Victor");
		rebelde.setIdade(22);
		rebelde.setGenero(Genero.HOMEM);
		rebelde.setLocalizacao(localizacao);
		rebelde.setInventario(inventario);
		
		return rebelde;
	}
}
